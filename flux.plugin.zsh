install_flux() {
    local whereami="$1"
    local bin="$HOME/.local/bin"
    local completions_dir="$whereami/src"

    if ! type flux 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -s https://fluxcd.io/install.sh >"$whereami/install.sh"
        chmod +x "$whereami/install.sh"
        "$whereami/install.sh" "$bin"

        mkdir -p "$completions_dir"
        flux completion zsh >"$completions_dir/_flux"
    fi

    fpath+="$completions_dir"
}

install_flux "${0:A:h}"
unset install_flux
